/////////////////////////////////////////////////
// Required ///
////////////////////////////////////////////////
var gulp = require('gulp'),
    browserify = require('gulp-browserify'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    plumber = require('gulp-plumber'),
    autoprefixer = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    replace = require('gulp-replace'),
    rename = require('gulp-rename');

/////////////////////////////////////////////////
// Script Tasks ///
////////////////////////////////////////////////
gulp.task('scripts', function(){
    gulp.src(['assets/js/**/*.js', '!assets/js/**/*.min.js'])
    .pipe(browserify({debug: true}))
    .pipe(plumber())
    .pipe(rename({suffix:'.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('assets/js'))
    .pipe(reload({stream:true}));
});


/////////////////////////////////////////////////
// Compass / Sass Tasks ///
////////////////////////////////////////////////
gulp.task('stylesheet', function(){
    return gulp.src('assets/scss/**/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('./css'))
    .pipe(rename('main.min.css'))
    .pipe(replace('/*!', '/*'))
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(plumber())
    .pipe(autoprefixer('last 2 versions'))
    .pipe(gulp.dest('assets/css/'))
    .pipe(reload({stream:true}));
});


/////////////////////////////////////////////////
// HTML Tasks ///
////////////////////////////////////////////////
gulp.task('html', function(){
    gulp.src('./*.html')
    .pipe(reload({stream:true}));
});



/////////////////////////////////////////////////
// Browser-Sync Tasks ///
////////////////////////////////////////////////
gulp.task('browser-sync', function(){
    browserSync({
      server:{
        baseDir: "./"
      }
    });
});


/////////////////////////////////////////////////
// Watch Tasks ///
////////////////////////////////////////////////
gulp.task('watch', function(){
    gulp.watch('assets/js/**/*.js', ['scripts']);
    gulp.watch('assets/scss/**/*.scss', ['stylesheet']);
    gulp.watch('./*.html', ['html']);
});


/////////////////////////////////////////////////
// Default Tasks ///
////////////////////////////////////////////////
gulp.task('default', ['scripts', 'stylesheet', 'html', 'browser-sync', 'watch']);
