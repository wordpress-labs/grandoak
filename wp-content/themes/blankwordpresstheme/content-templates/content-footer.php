<?php
/**
 * Section footer file for blank wordpress theme.
 * @package blankwordpresstheme
 */
?>
<footer id ="footer">
		<div class="footer-links">
				<div class="container">
						<div class="row">
								<?php dynamic_sidebar('bwt-footer'); ?>
						</div>
				</div>
		</div>
		<div class="footer-copyright">
				<div class="container">
						<div class="row">
								<div class="col-xs-12">
									<?php
									if(get_theme_mod('bwt_footer_text')):
										echo '<div class="footer-text">'.get_theme_mod('bwt_footer_text').'</div>';
									endif;
									?>
								</div>
						</div>
				</div>
		</div>
</footer>
