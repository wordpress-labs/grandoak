<?php
/**
 * Section header file for blank wordpress theme.
 * @package blankwordpresstheme
 */
?>

<div id="bwt-header">
		<div class="bwt-header-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<div class="bwt-logo">
				             <a href="<?php bloginfo('url');?>">
				               <?php if(get_theme_mod('bwt_logo')): ?>
				               <img src="<?php echo get_theme_mod('bwt_logo');?>" alt="<?php echo esc_attr(get_bloginfo('name', 'display'));?>" class="img-responsive" />
				               <?php else: ?>
				                 Blank Wordpress Theme
				              <?php endif; ?>
				             </a>
		           		</div>
		           		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				            <span class="sr-only">Toggle navigation</span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				        </button>
					</div>
					<div class="col-sm-9">
						<?php dynamic_sidebar('bwt-header-top'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="bwt-header-bottom">
			<div class="container">
				<div class="row">
					<div class="col-sm-3">&nbsp;</div>
					<div class="col-sm-9">
						<nav id="bwt-navigation" class="navbar">
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-animations">
								<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => 'div', 'container_class' => '', 'container_id' => '', 'menu_class' => 'nav nav-justified hidden-xs','walker'=> new wp_bootstrap_navwalker()) ); ?>
							</div>
						</nav>
					</div>
				</div>	
			</div>
		</div>
</div>

<nav class="navbar navbar-default visible-xs hidden">
        <div class="container-fluid">
          <div id="navbar" class="navbar-collapse collapse">
            <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => 'div', 'container_class' => '', 'container_id' => '', 'menu_class' => 'nav navbar-nav','walker'=> new wp_bootstrap_navwalker()) ); ?>
          </div>
        </div>
</nav>