<?php
/**
 * Main sidebar file for blank wordpress theme.
 * @package blankwordpresstheme
 *
 *
 * @since 0.0.1
 */

if (is_active_sidebar( 'bwt-sidebar' ) ) {
	return;
}
?>
